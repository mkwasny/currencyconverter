﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CurrencyConverter
{
   public class CurrencyConverter : ICurrencyConverter
   {
      private readonly ICurrencyTextBuilder _currencyTextBuilder;
      private readonly string _dollarsPattern = @"^([1-9]{1}([0-9]{1,2})?(\s{1}[0-9]{3}){0,2}|0)";
      private readonly string _centsPattern = @"(,[0-9]{1,2})?$";

      private string _inputValue;
      private decimal _numericValue;
      private int _dollars;
      private int _cents;

      public CurrencyConverter()
      {
         _currencyTextBuilder = new CurrencyTextBuilder();
      }

      public string ConvertToText(string inputValue)
      {
         _inputValue = inputValue;

         _currencyTextBuilder.Clear();

         if (!ValidateInputValue())
            throw new ArgumentException(nameof(inputValue));

         if (!ConvertInputValueToNumber())
            throw new ArgumentException(nameof(inputValue));

         RetrieveDollarsAndCents();

         if (!ConvertDollarsToText())
            throw new InvalidCastException();

         if (!ConvertCentsToText())
            throw new InvalidCastException();

         return GetValue();
      }

      private bool ValidateInputValue()
      {
         return Regex.IsMatch(_inputValue, $"{_dollarsPattern}{_centsPattern}");
      }

      private bool ConvertInputValueToNumber()
      {
         return decimal.TryParse(_inputValue, out _numericValue);
      }

      private void RetrieveDollarsAndCents()
      {
         _dollars = Convert.ToInt32(Math.Floor(_numericValue));
         _cents = Convert.ToInt16((_numericValue % 1) * 100);
      }

      private bool ConvertDollarsToText()
      {
         if (_dollars == 0)
         {
            _currencyTextBuilder.Add($"{Constant.Zero} {Constant.Dollars}");
            return true;
         }

         var match = Regex.Match(_inputValue, _dollarsPattern);

         if (match == null)
            return false;

         var dollarsSplitted = match.Value.Split(' ');

         for (int i = 0; i < dollarsSplitted.Length; i++)
         {
            int maxThreeDigitsInteger = Convert.ToInt16(dollarsSplitted[i]);

            ConvertNumberToText(maxThreeDigitsInteger, dollarsSplitted[i]);

            if (i == 0 && dollarsSplitted.Length == 3)
               _currencyTextBuilder.Add(Constant.Million);

            if (maxThreeDigitsInteger == 0)
               continue;

            if ((i == 1 && dollarsSplitted.Length == 3) || (i == 0 && dollarsSplitted.Length == 2))
               _currencyTextBuilder.Add(Constant.Thousand);
         }

         _currencyTextBuilder.Add(_dollars == 1 ? Constant.Dollar : Constant.Dollars);

         return true;
      }

      private bool ConvertCentsToText()
      {
         if (_cents == 0)
            return true;

         _currencyTextBuilder.Add(Constant.And);

         ConvertNumberToText(_cents, _cents.ToString());

         _currencyTextBuilder.Add(_cents == 1 ? Constant.Cent : Constant.Cents);

         return true;
      }

      private void ConvertNumberToText(int maxThreeDigitsInteger, string maxThreeDigitsIntegerInString)
      {
         ConvertHundreds(maxThreeDigitsInteger, maxThreeDigitsIntegerInString);
         ConvertTens(maxThreeDigitsInteger, maxThreeDigitsIntegerInString);
         ConvertUnits(maxThreeDigitsInteger, maxThreeDigitsIntegerInString);
      }

      private void ConvertHundreds(int maxThreeDigitsInteger, string maxThreeDigitsIntegerInString)
      {
         if (maxThreeDigitsIntegerInString.Length != 3)
            return;

         if (maxThreeDigitsInteger < 100)
            return;

         var firstDigit = maxThreeDigitsIntegerInString.ElementAt(0).ToString();
         _currencyTextBuilder.AddHundred(GetDigitAsString(firstDigit, false));
      }

      private void ConvertTens(int maxThreeDigitsInteger, string maxThreeDigitsIntegerInString)
      {
         if (maxThreeDigitsIntegerInString.Length < 2)
            return;

         if ((maxThreeDigitsInteger % 100) < 10)
            return;

         if ((maxThreeDigitsInteger % 100) >= 10 && (maxThreeDigitsInteger % 100) < 20)
         {
            var tensInString = maxThreeDigitsIntegerInString.Substring(maxThreeDigitsIntegerInString.Length - 2);
            _currencyTextBuilder.Add(GetDigitAsString(tensInString, false));
         }

         var secondDigit = maxThreeDigitsIntegerInString.Substring(maxThreeDigitsIntegerInString.Length - 2, 1);
         _currencyTextBuilder.AddToBuffer(GetDigitAsString(secondDigit, true));
      }

      private void ConvertUnits(int maxThreeDigitsInteger, string maxThreeDigitsIntegerInString)
      {
         if ((maxThreeDigitsIntegerInString.Length == 0) ||
            ((maxThreeDigitsInteger % 10) == 0) ||
            ((maxThreeDigitsInteger % 100) >= 10 && (maxThreeDigitsInteger % 100) < 20))
         {
            _currencyTextBuilder.AddFromBuffer();
            return;
         }

         if ((maxThreeDigitsInteger % 100) > 20 && (maxThreeDigitsInteger / 10) > 1)
            _currencyTextBuilder.AddToBuffer(Constant.Separator);

         var thirdDigit = maxThreeDigitsIntegerInString.Last().ToString();
         _currencyTextBuilder.AddToBuffer(GetDigitAsString(thirdDigit, false));

         _currencyTextBuilder.AddFromBuffer();
      }

      private string GetDigitAsString(string digitInString, bool getTens)
      {
         switch (digitInString)
         {
            case "0":
               return getTens ? "" : Constant.Zero;
            case "1":
               return getTens ? "" : Constant.One;
            case "2":
               return getTens ? Constant.Twenty : Constant.Two;
            case "3":
               return getTens ? Constant.Thirty : Constant.Three;
            case "4":
               return getTens ? Constant.Forty : Constant.Four;
            case "5":
               return getTens ? Constant.Fifty : Constant.Five;
            case "6":
               return getTens ? Constant.Sixty : Constant.Six;
            case "7":
               return getTens ? Constant.Seventy : Constant.Seven;
            case "8":
               return getTens ? Constant.Eighty : Constant.Eight;
            case "9":
               return getTens ? Constant.Ninety : Constant.Nine;
            case "10":
               return getTens ? "" : Constant.Ten;
            case "11":
               return getTens ? "" : Constant.Eleven;
            case "12":
               return getTens ? "" : Constant.Twelve;
            case "13":
               return getTens ? "" : Constant.Thirteen;
            case "14":
               return getTens ? "" : Constant.Fourteen;
            case "15":
               return getTens ? "" : Constant.Fifteen;
            case "16":
               return getTens ? "" : Constant.Sixteen;
            case "17":
               return getTens ? "" : Constant.Seventeen;
            case "18":
               return getTens ? "" : Constant.Eighteen;
            case "19":
               return getTens ? "" : Constant.Nineteen;
            default:
               return "";
         }
      }

      private string GetValue()
      {
         return _currencyTextBuilder.ToString();
      }
   }
}
