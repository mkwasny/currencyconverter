﻿using System.Collections.Generic;
using System.Text;

namespace CurrencyConverter
{
   public class CurrencyTextBuilder : ICurrencyTextBuilder
   {
      private readonly List<string> _currencyText;
      private readonly StringBuilder _stringBuilder;

      public CurrencyTextBuilder()
      {
         _currencyText = new List<string>();
         _stringBuilder = new StringBuilder();
      }

      public void Clear()
      {
         _currencyText.Clear();
         _stringBuilder.Clear();
      }

      public void Add(string text)
      {
         _currencyText.Add(text);
      }

      public void AddHundred(string text)
      {
         _currencyText.Add(text);
         _currencyText.Add(Constant.Hundred);
      }

      public void AddToBuffer(string text)
      {
         _stringBuilder.Append(text);
      }

      public void AddFromBuffer()
      {
         var text = _stringBuilder.ToString();
         _stringBuilder.Clear();

         if (!string.IsNullOrEmpty(text))
            _currencyText.Add(text);
      }

      public override string ToString()
      {
         return string.Join(" ", _currencyText);
      }
   }
}
