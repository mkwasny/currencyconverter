﻿namespace CurrencyConverter
{
   public interface ICurrencyTextBuilder
   {
      void Add(string text);
      void AddFromBuffer();
      void AddHundred(string text);
      void AddToBuffer(string text);
      void Clear();
      string ToString();
   }
}