﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter
{
   public static class Constant
   {
      public static readonly string Dollar = "dollar";
      public static readonly string Dollars = "dollars";
      public static readonly string Cent = "cent";
      public static readonly string Cents = "cents";
      public static readonly string Million = "million";
      public static readonly string Thousand = "thousand";
      public static readonly string And = "and";
      public static readonly string Zero = "zero";
      public static readonly string One = "one";
      public static readonly string Two = "two";
      public static readonly string Three = "three";
      public static readonly string Four = "four";
      public static readonly string Five = "five";
      public static readonly string Six = "six";
      public static readonly string Seven = "seven";
      public static readonly string Eight = "eight";
      public static readonly string Nine = "nine";
      public static readonly string Ten = "ten";
      public static readonly string Eleven = "eleven";
      public static readonly string Twelve = "twelve";
      public static readonly string Thirteen = "thirteen";
      public static readonly string Fourteen = "fourteen";
      public static readonly string Fifteen = "fifteen";
      public static readonly string Sixteen = "sixteen";
      public static readonly string Seventeen = "seventeen";
      public static readonly string Eighteen = "eighteen";
      public static readonly string Nineteen = "nineteen";
      public static readonly string Twenty = "twenty";
      public static readonly string Thirty = "thirty";
      public static readonly string Forty = "forty";
      public static readonly string Fifty = "fifty";
      public static readonly string Sixty = "sixty";
      public static readonly string Seventy = "seventy";
      public static readonly string Eighty = "eighty";
      public static readonly string Ninety = "ninety";
      public static readonly string Hundred = "hundred";
      public static readonly string Separator = "-";

   }
}
