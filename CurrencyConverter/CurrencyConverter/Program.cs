﻿using System;

namespace CurrencyConverter
{
   class Program
   {
      static ICurrencyConverter currencyConverter;

      static void Main()
      {
         currencyConverter = new CurrencyConverter();

         while (true)
         {
            Console.WriteLine("Welcome to the Currency Converter!");
            Console.WriteLine("Insert 'h' for help, 'i' for info, 'e' to exit the program.");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Please insert the number: ");

            string inputValue = Console.ReadLine();

            switch (inputValue)
            {
               case "e":
                  return;
               case "h":
                  ShowHelp();
                  break;
               case "i":
                  ShowInfo();
                  break;
               default:
                  ConvertCurrency(inputValue);
                  break;
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.Clear();
         }
      }

      static void ShowHelp()
      {
         Console.Clear();
         Console.WriteLine("HELP");
         Console.WriteLine();
         Console.WriteLine("-> The maximum number is 999 999 999.");
         Console.WriteLine("-> The maximum number of cents is 99.");
         Console.WriteLine("-> The separator between dollars and cents is ',' (comma).");
         Console.WriteLine("-> The space is required between millions, thousands and hundreds.");
      }

      static void ShowInfo()
      {
         Console.Clear();
         Console.WriteLine("INFO");
         Console.WriteLine();
         Console.WriteLine("Console program which converts currency (dollars) from numbers into word presentation.");
         Console.WriteLine();
         Console.WriteLine("Created for interview purpose by Michal Kwasny.");
      }

      static void ConvertCurrency(string inputValue)
      {
         try
         {
            Console.WriteLine(currencyConverter.ConvertToText(inputValue));
         }
         catch (ArgumentException)
         {
            Console.WriteLine("Invalid input");
         }
         catch (Exception)
         {
            Console.WriteLine("Unknown conversion problem");
         }
      }
   }
}
