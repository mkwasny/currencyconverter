﻿namespace CurrencyConverter
{
   public interface ICurrencyConverter
   {
      string ConvertToText(string inputValue);      
   }
}