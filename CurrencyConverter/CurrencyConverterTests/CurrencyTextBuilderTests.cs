﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CurrencyConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyConverter.Tests
{
   [TestClass()]
   public class CurrencyTextBuilderTests
   {
      private readonly string testText = "text";

      private ICurrencyTextBuilder _currencyTextBuilder;

      [TestInitialize]
      public void TestInitialize()
      {
         _currencyTextBuilder = new CurrencyTextBuilder();
      }

      [TestMethod()]
      public void AddTest()
      {
         _currencyTextBuilder.Add(testText);

         Assert.AreEqual(_currencyTextBuilder.ToString(), testText);
      }

      [TestMethod()]
      public void ClearTest()
      {
         _currencyTextBuilder.Add(testText);
         _currencyTextBuilder.Clear();

         Assert.AreEqual(_currencyTextBuilder.ToString(), "");
      }

      [TestMethod()]
      public void ClearBuffer()
      {
         _currencyTextBuilder.AddToBuffer(testText);
         _currencyTextBuilder.Clear();
         _currencyTextBuilder.AddFromBuffer();

         Assert.AreEqual(_currencyTextBuilder.ToString(), "");
      }

      [TestMethod()]
      public void AddHundredTest()
      {
         _currencyTextBuilder.AddHundred(testText);

         Assert.AreEqual(_currencyTextBuilder.ToString(), $"{testText} hundred");
      }

      [TestMethod()]
      public void BufferTest()
      {
         _currencyTextBuilder.AddToBuffer(testText);
         _currencyTextBuilder.AddToBuffer(testText);
         _currencyTextBuilder.AddFromBuffer();

         Assert.AreEqual(_currencyTextBuilder.ToString(), $"{testText}{testText}");
      }

      [TestMethod()]
      public void ToStringTest()
      {
         _currencyTextBuilder.Add(testText);
         _currencyTextBuilder.Add(testText);

         Assert.AreEqual(_currencyTextBuilder.ToString(), $"{testText} {testText}");
      }
   }
}