﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CurrencyConverter.Tests
{
   [TestClass()]
   public class CurrencyConverterTests
   {
      private ICurrencyConverter _currencyConverter;

      [TestInitialize]
      public void TestInitialize()
      {
         _currencyConverter = new CurrencyConverter();
      }

      [TestMethod()]
      public void ConvertToTextInputValidationTest()
      {
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("f"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("0j"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("0 988"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("1 000 000 000"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("67,900"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("67 87"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("43,if"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("01"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("1 ,8"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("1, 8"));
         Assert.ThrowsException<ArgumentException>(() => _currencyConverter.ConvertToText("1,"));
      }

      [TestMethod()]
      public void ConvertToTextTest()
      {
         Assert.AreEqual(
            _currencyConverter.ConvertToText("0"),
            "zero dollars");

         Assert.AreEqual(
            _currencyConverter.ConvertToText("1"),
            "one dollar");

         Assert.AreEqual(
            _currencyConverter.ConvertToText("25,1"),
            "twenty-five dollars and ten cents");

         Assert.AreEqual(
            _currencyConverter.ConvertToText("0,01"),
            "zero dollars and one cent");

         Assert.AreEqual(
            _currencyConverter.ConvertToText("45 100"),
            "forty-five thousand one hundred dollars");

         Assert.AreEqual(
            _currencyConverter.ConvertToText("999 999 999,99"),
            "nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents");
      }
   }
}